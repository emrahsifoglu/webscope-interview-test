export default class Team {

  id: number;
  name: string;
  members: number[];

  constructor(id: number, name: string, members: number[] = []) {
    this.id = id;
    this.name = name;
    this.members = members;
  }

  setId = (id: number) => {
    this.id = id;
  }

  setName = (name: string) => {
    this.name = name;
  }

  getMembers = () => {
    return this.members;
  }

  setMembers = (members: number[]) => {
    this.members = members;
  }

}
