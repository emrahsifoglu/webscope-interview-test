# Webscope interview test

## Table of Contents

* [Getting Started](#getting-started)
  * [Prerequisite](#prerequisite)
  * [Installation](#installation)
* [Running](#running)
* [Built with](#built-with)
  * [Development Environment](#development-environment)
* [Author](#author)
* [License](#license)
* [Resources](#resources)

## Getting Started

These instructions will guide you to up and running the project on your local machine.

### Prerequisite

* Node.js 
* Yarn (or npm)

### Installation

You can run `yarn install` to install dependencies.

## Running

You can start the development with `npm start`.

| Method | Path                         | Info                   |
| ------ | ---------------------------  | -----------------------|
| GET | [/](http://localhost:3000) | home page |
| GET | [/team](http://localhost:3000/team) | team list |
| GET | [/author](http://localhost:3000/author) | author list |
| GET | [/article](http://localhost:3000/article) | article list |

## Testing

There are no tests to run

## Built with

#### Development Environment
* [Ubuntu](https://www.ubuntu.com/download/server) - Server
* [Linux Mint 18](https://www.linuxmint.com/) - OS
* [WebStorm](https://www.jetbrains.com/webstorm/) - IDE
* [Chromium](https://www.chromium.org/Home) - Browser

## Author

* **Emrah Sifoğlu** - *Initial work* - [emrahsifoglu](- https://github.com/emrahsifoglu)

## License

This project is licensed under the [MIT License](http://opensource.org/licenses/MIT).

## Resources

- http://www.penta-code.com/getting-started-with-react-with-create-react-app/
- https://github.com/mpontus/create-react-app-hot-loader
- http://joshbroton.com/add-react-hot-reloading-create-react-app/
- https://www.youtube.com/watch?v=f-ctxG2qEps
- https://www.youtube.com/watch?v=GIMjnWuA9B0
- https://github.com/gaearon/react-hot-loader/issues/243
- https://github.com/wmonk/create-react-app-typescript/tree/master/packages
- https://github.com/Microsoft/TypeScript-React-Starter
- https://github.com/vitaliy-bobrov/angular-hot-loader/issues/5
- https://github.com/ReactTraining/react-router/issues/4645
- https://v4-alpha.getbootstrap.com/examples/sticky-footer-navbar/#
- https://stackoverflow.com/questions/39063396/error-bootstraps-javascript-requires-jquery-using-webpack
- https://stackoverflow.com/questions/1026069/how-do-i-make-the-first-letter-of-a-string-uppercase-in-javascript
- https://www.linkedin.com/pulse/javascript-find-object-array-based-objects-property-rafael
