import * as React from 'react';
import Authors from '../../containers/Authors';

export class AuthorIndexPage extends React.Component<{}, {}> {
    render() {
        return (
            <div className="page">
                <div className="page-header author-page-header">
                    Authors Page
                </div>
                <div className="content">
                    <Authors/>
                </div>
            </div>
        );
    }
}

export default AuthorIndexPage;
