import * as React from 'react';

interface ArticleListItemProp {
    id: number;
    text: string;
    index: number;
}

export class ArticleListItem extends React.Component<ArticleListItemProp, {}> {
    render() {
        let { id, text, index } = this.props;
        let classNames  = index % 2 === 0 ? 'first' : 'second';

        return (
            <div className={('row list-item ' + (classNames))}>
                <div className="col-xs-2">{id}</div>
                <div className="col-xs-10">
                    {text}
                </div>
            </div>
        );
    }
}
