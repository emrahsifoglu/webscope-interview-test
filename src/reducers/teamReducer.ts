import * as types from '../constants/actionTypes';
import Team from '../models/Team/Team';

export const teamReducer = (state: Team, action: any) => {
    switch (action.type) {
        case types.LOAD_TEAM_SUCCESS:
            return action.team;
        default:
            return {};
    }
};

export default teamReducer;
