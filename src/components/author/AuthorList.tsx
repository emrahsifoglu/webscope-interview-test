import * as React from 'react';
import Author from '../../models/Author/Author';
import { AuthorListItem } from './AuthorListItem';

interface AuthorListProp {
    authors: Author[];
}

export class AuthorList extends React.Component<AuthorListProp, {}> {
    render() {
        let { authors } = this.props;

        return (
            <div className="list author-list">
                <div className="row">
                    <div className="col-xs-2 pull-left">Id</div>
                    <div className="col-xs-10 list-title author-list-title">Name</div>
                </div>
                {authors.map((author: Author, index: number) => {
                    return (<AuthorListItem
                        key={index}
                        index={index}
                        {...author}
                    />);
                })}
            </div>
        );
    }
}
