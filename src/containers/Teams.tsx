import * as React from 'react';
import { connect } from 'react-redux';
import { TeamList } from '../components/team/TeamList';
import Team from '../models/Team/Team';

interface TeamsProps {
    teams: Team[];
}

export class Teams extends React.Component<TeamsProps, {}> {
    render() {

        let { teams } = this.props;

        return (
            <TeamList teams={teams} />
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        teams: state.teams
    };
};

export default connect(mapStateToProps)(Teams);
