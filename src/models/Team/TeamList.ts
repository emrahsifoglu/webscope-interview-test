import Team from './Team';

let list: Team[] = [];

export default class TeamList {

    constructor(teams: {}[]) {
        list = teams.map((team:  { id: number, name: string, members: number[] } ) => {
            return new Team(team.id, team.name, team.members);
        });
    }

    toArray() {
        return list;
    }

}
