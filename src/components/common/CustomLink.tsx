import * as React from 'react';
import { Link } from 'react-router-dom';
import { object } from 'prop-types';

interface CustomLinkProps {
    to: string;
}

export default class extends React.Component<CustomLinkProps, {}> {
    static contextTypes = {
        router: object.isRequired
    };

    render() {
        let isActive = this.context.router.route.location.pathname === this.props.to;
        let className = isActive ? 'active' : '';

        return(
            <li className={className}><Link {...this.props}>{this.props.children}</Link></li>
        );
    }
}
