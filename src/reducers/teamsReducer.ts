import * as types from '../constants/actionTypes';
import Team from '../models/Team/Team';

export const teamsReducer = (state: Team[] = [], action: any) => {
    switch (action.type) {
        case types.LOAD_TEAMS_SUCCESS:
            return action.teams;
        default:
            return state;
    }
};

export default teamsReducer;
