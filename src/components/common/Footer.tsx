import * as React from 'react';

export const Footer = () => (
    <footer className="footer">
        <div className="container">
            <p className="text-muted text-center">footer content</p>
        </div>
    </footer>
);
