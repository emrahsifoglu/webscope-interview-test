import { AppContainer } from 'react-hot-loader';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import App from './components/App';

declare const module: any;

const rootEl = document.getElementById('root');

const render = (Component: any) =>
    ReactDOM.render(
        <AppContainer>
            <Component />
        </AppContainer>,
        rootEl
    );

render(App);

if (module.hot) {
    module.hot.accept('./components/App', () => render(App)); // render(require('./components/App').default));
}

registerServiceWorker();

/*
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import App from './App';
import './index.css';

function renderApp() {
    ReactDOM.render(
        <App/>,
        document.getElementById('root')
    );
}

if (module.hot) {
    module.hot.accept();
    renderApp();
} else {
    renderApp();
}

registerServiceWorker();
*/