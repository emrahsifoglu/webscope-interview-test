import * as types from '../constants/actionTypes';
import Author from '../models/Author/Author';

export const authorsReducer = (state: Author[] = [], action: any) => {
    switch (action.type) {
        case types.LOAD_AUTHORS_SUCCESS:
            return action.authors;
        default:
            return state;
    }
};

export default authorsReducer;
