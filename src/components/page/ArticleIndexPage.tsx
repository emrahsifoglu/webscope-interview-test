import * as React from 'react';
import Articles from '../../containers/Articles';

export class ArticleIndexPage extends React.Component<{}, {}> {
    render() {
        return (
            <div className="page">
                <div className="page-header article-page-header">
                    Articles Page
                </div>
                <div className="content">
                    <Articles/>
                </div>
            </div>
        );
    }
}

export default ArticleIndexPage;
