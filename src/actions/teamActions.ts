import * as actions from '../constants/actionTypes';
import Team from '../models/Team/Team';
import TeamList from '../models/Team/TeamList';
import { Dispatch } from 'redux';
import { findById } from '../functions';

const teamList = [
    { id: 1, name: 'webscope 1', members: [1, 2, 3, 4] },
    { id: 2, name: 'webscope 2', members: [5, 6, 7] },
];

export function loadTeamsSuccess(teams: Team[] = []) {
    return {
        type: actions.LOAD_TEAMS_SUCCESS, teams
    };
}

export function loadTeamSuccess(team: Team) {
    return {
        type: actions.LOAD_TEAM_SUCCESS, team
    };
}

export const loadTeams = () => {
    return (dispatch: Dispatch<string>) => {
        dispatch(loadTeamsSuccess(new TeamList(teamList).toArray()));
    };
};

export const loadTeam = (id: number) => {
    return (dispatch: Dispatch<string>) => {
        let team = findById(teamList, id);
        dispatch(loadTeamSuccess(new Team(team.id, team.name, team.members)));
    };
};
