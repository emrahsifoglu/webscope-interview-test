import { combineReducers } from 'redux';
import team from './teamReducer';
import teams from './teamsReducer';
import authors from './authorsReducer';
import articles from './articlesReducer';

export const rootReducer = combineReducers({
    team,
    teams,
    authors,
    articles
});

export default rootReducer;
