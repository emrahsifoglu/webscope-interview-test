export const firstToUpper = (str: string) => str.charAt(0).toUpperCase() + str.slice(1);

export const findById = (arr: any, id: number) => arr.find(function (item: any) { return item.id === Number(id); });

export const findByIds = (arr: any, ids: number[]) => arr.filter((item: any) => { return ids.indexOf(item.id) > -1; });
