import Article from '../Article/Article';

export default class Author {

  id: number;
  name: string;
  articles: Article[] = [];

  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }

  setId = (id: number) => {
    this.id = id;
  }

  setName = (name: string) => {
    this.name = name;
  }

  addArticle = (article: Article) => {
      this.articles.push(article);
  }

  removeArticles = () => {
    this.articles = [];
  }

  hasOnlyOneArticle = () => {
    return this.articles.length === 1;
  }

  hasArticles = () => {
    return this.articles.length > 0;
  }

  getMemo = () => {
    let wrote = 'wrote 0 article';

    if (this.hasArticles()) {
        wrote =  this.hasOnlyOneArticle() ? 'an article ' : 'wrote articles ';

        let texts: string[] = [];
        let articles = this.articles.slice(0, 2);
        let count = this.articles.length;
        let end = count - 2;
        let more  = end > 0 ? ' and ' + end + ' more' : '';

        articles.forEach(function (article: Article) {
            texts.push(article.text);
        });

        wrote += texts.join(', ') + more;
    }

    return this.name + ' ' + wrote;
  }

}
