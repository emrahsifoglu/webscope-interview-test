import * as actions from '../constants/actionTypes';
import Author from '../models/Author/Author';
import AuthorList from '../models/Author/AuthorList';
import { Dispatch } from 'redux';

export function loadAuthorsSuccess(authors: Author[] = []) {
    return {
        type: actions.LOAD_AUTHORS_SUCCESS, authors
    };
}

export const loadAuthors = () => {
    return (dispatch: Dispatch<string>) => {
        const authors = [
            {id: 1, name: 'oliver'},
            {id: 2, name: 'jan'},
            {id: 3, name: 'jakub'},
            {id: 4, name: 'peter'},
            {id: 5, name: 'tomas'},
            {id: 6, name: 'drahoslav'},
            {id: 7, name: 'honza'},
        ];
        dispatch(loadAuthorsSuccess(new AuthorList(authors).toArray()));
    };
};
