import * as React from 'react';
import { firstToUpper } from '../../functions';

interface AuthorListItemItemProp {
    id: number;
    name: string;
    index: number;
}

export class AuthorListItem extends React.Component<AuthorListItemItemProp, {}> {
    render() {
        let { id, name, index } = this.props;
        let classNames  = index % 2 === 0 ? 'first' : 'second';

        return (
            <div className={('row list-item ' + (classNames))}>
                <div className="col-xs-2">{id}</div>
                <div className="col-xs-10">
                    { firstToUpper(name) }
                </div>
            </div>
        );
    }
}
