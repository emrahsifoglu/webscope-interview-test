import Author from '../Author/Author';

export default class Article {

  id: number;
  text: string;
  authors: number[];

  constructor(id: number, name: string, authors: number[] = []) {
    this.id = id;
    this.text = name;
    this.authors = authors;
  }

  setId = (id: number) => {
    this.id = id;
  }

  setText = (text: string) => {
    this.text = text;
  }

  setAuthors = (authors: number[]) => {
      this.authors = authors;
  }

  hasAuthor = (author: Author) => {
    return this.authors.indexOf(author.id) > -1;
  }

}
