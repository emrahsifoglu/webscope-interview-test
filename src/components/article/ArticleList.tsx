import * as React from 'react';
import Article from '../../models/Article/Article';
import { ArticleListItem } from './ArticleListItem';

interface ArticleListProp {
    articles: Article[];
}

export class ArticleList extends React.Component<ArticleListProp, {}> {
    render() {
        let { articles } = this.props;

        return (
            <div className="list article-list">
                <div className="row">
                    <div className="col-xs-2 pull-left">Id</div>
                    <div className="col-xs-10 list-title article-list-title">Name</div>
                </div>
                {articles.map((article: Article, index: number) => {
                    return (<ArticleListItem
                        key={index}
                        index={index}
                        {...article}
                    />);
                })}
            </div>
        );
    }
}
