import * as React from 'react';
import { connect } from 'react-redux';
import { loadTeam } from '../actions/teamActions';
import Team from '../models/Team/Team';
import Article from '../models/Article/Article';
import Author from '../models/Author/Author';
import { findByIds, firstToUpper} from '../functions';

interface TeamProps {
    id: number;
    getTeam: Function;
    team: Team;
    articles: Article[];
    authors: Author[];
}

class TeamView extends React.Component<TeamProps, {}> {
    constructor(props: any, context: any) {
        super(props, context);
    }

    componentDidMount() {
        let { getTeam, id } = this.props;
        getTeam(id);
    }

    render() {
        let members = this.props.team.members;
        let articles = this.props.articles;
        let authors: Author[] = [];

        if (members && articles) {
            authors = findByIds(this.props.authors, members);
            authors.map(function (author: Author) {
                author.removeArticles();
                articles.forEach(function (article: Article) {
                    if (article.hasAuthor(author)) {
                        author.addArticle(article);
                    }
                });
            });
        }

        return (
            <div>
                <p><span>{this.props.team.name}</span></p>
                <div>
                    <ul className="list-unstyled">
                        {authors.map((author: Author, index: number) => {
                            return (<li key={index}>{firstToUpper(author.getMemo())}</li>);
                        })}
                    </ul>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any, ownProps: any) => {
    return {
        team: state.team,
        authors: state.authors,
        articles: state.articles,
    };
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        getTeam: (id: number) => {
            dispatch(loadTeam(id));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TeamView);
