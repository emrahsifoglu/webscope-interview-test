import Article from './Article';

let list: Article[] = [];

export default class ArticleList {

    constructor(articles: {}[]) {
        list = articles.map((article: { id: number, text: string, authors: number[] } ) => {
            return new Article(article.id, article.text, article.authors);
        });
    }

    toArray() {
        return list;
    }

}
