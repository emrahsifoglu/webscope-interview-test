import * as types from '../constants/actionTypes';
import Article from '../models/Article/Article';

export const articlesReducer = (state: Article[] = [], action: any) => {
    switch (action.type) {
        case types.LOAD_ARTICLES_SUCCESS:
            return action.articles;
        default:
            return state;
    }
};

export default articlesReducer;
