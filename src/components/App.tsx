import * as React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { configureStore } from '../store/configureStore';
import { Header } from './common/Header';
import { Footer } from './common/Footer';
import { HomePage } from './page/HomePage';
import { TeamIndexPage } from './page/TeamIndexPage';
import { TeamPage } from './page/TeamPage';
import { AuthorIndexPage } from './page/AuthorIndexPage';
import { ArticleIndexPage } from './page/ArticleIndexPage';
import { loadTeams } from '../actions/teamActions';
import { loadAuthors } from '../actions/authorActions';
import { loadArticles } from '../actions/articleActions';
import 'font-awesome/css/font-awesome.css';
import 'bootstrap/dist/css/bootstrap.css';
import '../styles/app.css';
import 'jquery/src/jquery';
import 'bootstrap/dist/js/bootstrap.js';

let store = configureStore();

const Main = () => (
    <Switch>
        <Route exact path="/" component={HomePage}/>
        <Route path="/team/:id" component={TeamPage}/>
        <Route path="/team" component={TeamIndexPage}/>
        <Route path="/author" component={AuthorIndexPage}/>
        <Route path="/article" component={ArticleIndexPage}/>
    </Switch>
);

const Body = () => (
    <div className="body">
        <div className="container">
            <Main/>
        </div>
    </div>
);

export default class App extends React.Component<{}, {}> {
    componentDidMount() {
        store.dispatch(loadTeams());
        store.dispatch(loadAuthors());
        store.dispatch(loadArticles());
    }

    render() {
        return (
            <Provider store={store}>
                <Router>
                    <div>
                        <Header />
                        <Body />
                        <Footer/>
                    </div>
                </Router>
            </Provider>
        );
    }
}