import * as React from 'react';
import Teams from '../../containers/Teams';

export class TeamIndexPage extends React.Component<{}, {}> {
    render() {
        return (
            <div className="page">
                <div className="page-header team-page-header">
                    Teams Page
                </div>
                <div className="content">
                    <Teams/>
                </div>
            </div>
        );
    }
}

export default TeamIndexPage;
