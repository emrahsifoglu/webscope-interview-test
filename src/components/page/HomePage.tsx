import * as React from 'react';

export class HomePage extends React.Component<{}, {}> {
    render() {
        return (
            <div className="page">
                <div className="page-header home-page-header">
                    Home Page
                </div>
                <div className="content">
                    Home Page Content
                </div>
            </div>
        );
    }
}

export default HomePage;
