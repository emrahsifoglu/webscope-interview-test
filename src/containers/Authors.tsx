import * as React from 'react';
import { connect } from 'react-redux';
import { AuthorList } from '../components/author/AuthorList';
import Author from '../models/Author/Author';

interface AuthorsProps {
    authors: Author[];
}

export class Authors extends React.Component<AuthorsProps, {}> {
    render() {

        let { authors } = this.props;

        return (
            <AuthorList authors={authors} />
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        authors: state.authors
    };
};

export default connect(mapStateToProps)(Authors);
