import * as React from 'react';
import { Link } from 'react-router-dom';
import CustomLink from './CustomLink';

export const Header = () => (
    <nav className="navbar navbar-default">
        <div className="container-fluid">
            <div className="navbar-header">
                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                </button>
                <Link className="navbar-brand" to="/">webscope-interview-test</Link>
            </div>
            <div className="collapse navbar-collapse" id="myNavbar">
                <ul className="nav navbar-nav">
                    <CustomLink to="/team">Teams</CustomLink>
                    <CustomLink to="/author">Authors</CustomLink>
                    <CustomLink to="/article">Articles</CustomLink>
                </ul>
            </div>
        </div>
    </nav>
);
