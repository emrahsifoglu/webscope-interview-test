import Author from './Author';

let list: Author[] = [];

export default class AuthorList {

    constructor(authors: {}[]) {
        list = authors.map((author: { id: number, name: string } ) => {
            return new Author(author.id, author.name);
        });
    }

    toArray() {
        return list;
    }

}
