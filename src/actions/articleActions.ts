import * as actions from '../constants/actionTypes';
import Article from '../models/Article/Article';
import ArticleList from '../models/Article/ArticleList';
import { Dispatch } from 'redux';

export function loadArticlesSuccess(articles: Article[] = []) {
    return {
        type: actions.LOAD_ARTICLES_SUCCESS, articles
    };
}

export const loadArticles = () => {
    return (dispatch: Dispatch<string>) => {
        const articles = [
            { id: 1, text: 'MobX in practise', authors: [3] },
            { id: 33, text: 'RxJS and redux-observable', authors: [1, 2, 3, 5, 6, 7] },
            { id: 23, text: 'Firebase', authors: [7, 2, 3]},
            { id: 333, text: 'Really cool article'},
            { id: 1234, text: 'Ramda.js and Redux combined', authors: [2] },
            { id: 2, text: 'CSS in JS', authors: [3, 5] },
        ];
        dispatch(loadArticlesSuccess(new ArticleList(articles).toArray()));
    };
};
