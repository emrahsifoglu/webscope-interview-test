import * as React from 'react';
import TeamView from '../../containers/TeamView';

export class TeamPage extends React.Component<any, {}> {
    render() {

        let id = this.props.match.params.id;

        return (
            <div className="page">
                <div className="page-header team-page-header">
                    Team Page
                </div>
                <div className="content">
                    <TeamView id={id}/>
                </div>
            </div>
        );
    }
}

export default TeamPage;
