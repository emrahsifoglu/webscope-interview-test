import * as React from 'react';
import { firstToUpper } from '../../functions';
import { Link } from 'react-router-dom';

interface TeamListItemProp {
    id: number;
    name: string;
    index: number;
}

export class TeamListItem extends React.Component<TeamListItemProp, {}> {
    render() {
        let { id, name, index } = this.props;
        let classNames  = index % 2 === 0 ? 'first' : 'second';

        return (
            <div className={('row list-item ' + (classNames))}>
                <div className="col-xs-2">{id}</div>
                <div className="col-xs-10">
                    <Link to={('/team/') + id}>{firstToUpper(name)}</Link>
                </div>
            </div>
        );
    }
}
