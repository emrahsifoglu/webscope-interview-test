import * as React from 'react';
import { connect } from 'react-redux';
import { ArticleList } from '../components/article/ArticleList';
import Article from '../models/Article/Article';

interface ArticlesProps {
    articles: Article[];
}

export class Articles extends React.Component<ArticlesProps, {}> {
    render() {

        let { articles } = this.props;

        return (
            <ArticleList articles={articles} />
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        articles: state.articles
    };
};

export default connect(mapStateToProps)(Articles);
