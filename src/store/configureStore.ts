import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers/rootReducer';
import * as thunk from 'redux-thunk';

const logger = (store: any) => (next: any) => (action: any) => {
  console.group(action.type);
  console.log('%c prev state', 'color:gray', store.getState());
  console.info('%c dispatching', 'color:blue', action);
  let result = next(action);
  console.log('%c next state', 'color:green', store.getState());
  console.groupEnd();

  return result;
};

const crashReporter = (store: any) => (next: any) => (action: any) => {
    try {
        return next(action);
    } catch (err) {
        console.error('Caught an exception!', err);
        throw err;
    }
};

export const configureStore = () => createStore(rootReducer, applyMiddleware(thunk.default, logger, crashReporter));
