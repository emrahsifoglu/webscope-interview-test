import * as React from 'react';
import Team from '../../models/Team/Team';
import { TeamListItem } from './TeamListItem';

interface TeamListProp {
    teams: Team[];
}

export class TeamList extends React.Component<TeamListProp, {}> {
    render() {
        let { teams } = this.props;

        return (
            <div className="list team-list">
                <div className="row">
                    <div className="col-xs-2 pull-left">Id</div>
                    <div className="col-xs-10 list-title team-list-title">Name</div>
                </div>
                {teams.map((team: Team, index: number) => {
                    return (<TeamListItem
                        key={index}
                        index={index}
                        {...team}
                    />);
                })}
            </div>
        );
    }
}
